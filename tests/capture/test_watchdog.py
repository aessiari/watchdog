from pathlib import Path

import pytest
import pandas as pd


@pytest.fixture(scope='session')
def raw_data():
    path = Path(__file__).parent / 'data' / 'watchdog' / 'temp.json'

    return pd.read_json(path)


@pytest.fixture(scope='class')
def pipeline_params():
    from sc.capture.sources.watchdog import Analyzer

    return {'event_cls': Analyzer.event_model}


@pytest.fixture(scope='class')
def pipeline(pipeline_params):
    from sc.capture.sources.watchdog import ProcessRawEvents

    return ProcessRawEvents(**pipeline_params)


def test_raw_data_is_dataframe(raw_data):
    assert isinstance(raw_data, pd.DataFrame)


def test_raw_data_is_not_empty(raw_data):
    assert not raw_data.empty


@pytest.mark.usefixtures('raw_data', 'pipeline')
class TestAnalysisPipeline:

    @pytest.fixture
    def output(self, raw_data, pipeline):
        return pipeline.run(raw_data)

    def test_expected_events_in_output(self, output):

        test_event = output[0]

        assert test_event.artifact.path == '/tmp/scicap/bar.txt'
        assert test_event.action == 'move'

    def test_directory_modified_events_not_in_output(self, output):

        def is_dir_modified(e):
            return e.artifact.type == 'directory' and e.action == 'write'

        matching = [e for e in output if is_dir_modified(e)]

        assert len(matching) == 0
