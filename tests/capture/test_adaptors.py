import pytest

from sc.capture.base import Adaptor


@pytest.fixture
def registry():
    return Adaptor.REGISTRY


@pytest.mark.parametrize('source', [
    'dirscan',
    'watchdog',
    'inotify',
    'strace'
])
def test_default_adaptors_are_available_after_importing_source_package(registry, source):
    from sc.capture import sources

    assert source in registry
    adaptor = registry[source]
    assert issubclass(adaptor, Adaptor)


@pytest.fixture
def new_adaptor_class():
    class TestAdaptor(Adaptor):
        source = 'test_source'

        def configure(self, **kwargs):
            ...

    return TestAdaptor


def test_new_adaptor_subclasses_are_available_to_registry(registry, new_adaptor_class):
    source = new_adaptor_class.source
    assert source in registry
    assert registry[source] == new_adaptor_class
