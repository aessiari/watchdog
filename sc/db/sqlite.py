
import os
import sqlite3
from sc.db.schema import SnapshotAttribute, SnapshotsDatabase, SnapshotTree
from sc.config import CONFIG_DIR

class SqlStore():
    def __init__(self):
        if not os.path.exists(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)
        dbfile = os.path.join(CONFIG_DIR, SnapshotsDatabase.DB)
        self.conn = sqlite3.connect(dbfile)
        self.snapshots_table = SnapshotsDatabase.SNAPSHOTS
        self.branches_table = SnapshotsDatabase.BRANCHES
        self.snapshots_attribs = [SnapshotAttribute.NAME, SnapshotAttribute.ALIAS,
                                  SnapshotAttribute.TIME, SnapshotAttribute.PARENT,
                                  SnapshotAttribute.BRANCH]
        self.branch_attribs = [SnapshotTree.BRANCH, SnapshotTree.CURRENT]

    def create(self):
        create_snapshots_table_stmt = "CREATE TABLE " + self.snapshots_table + " (" + \
                                      SnapshotAttribute.NAME + " TEXT, " + \
                                      SnapshotAttribute.ALIAS + " TEXT, " + \
                                      SnapshotAttribute.TIME + " DATETIME, " + \
                                      SnapshotAttribute.PARENT + " TEXT, " + \
                                      SnapshotAttribute.BRANCH + " TEXT)"
        create_branches_table_stmt = "CREATE TABLE " + self.branches_table + " (" + \
                                     SnapshotTree.BRANCH + " TEXT UNIQUE, " + \
                                     SnapshotTree.CURRENT + " TEXT)"
        c = self.conn.cursor()
        c.execute(create_snapshots_table_stmt)
        c.execute(create_branches_table_stmt)

    def put(self, values):
        c = self.conn.cursor()
        columns = ",".join(self.snapshots_attribs)
        value_str = ",".join(['?' for _ in range(len(self.snapshots_attribs))])
        stmt = "INSERT INTO " + self.snapshots_table + " (" + columns + ") VALUES (" + value_str + ")"
        c.executemany(stmt, values)
        columns = ','.join(self.branch_attribs)
        value_str = ",".join(['?' for _ in range(len(self.branch_attribs))])
        stmt = "INSERT OR REPLACE INTO " + self.branches_table + " (" + columns + ") VALUES (" + value_str + ")"
        branch_values = []
        for v in values:
            branch_values.append((v[-1], v[0]))
        c.executemany(stmt, branch_values)
        self.conn.commit()

    def get(self, attributes, limit=None):
        c = self.conn.cursor()
        columns = ",".join(attributes)
        stmt = "SELECT " + columns + " FROM " + self.snapshots_table
        if limit is not None:
            stmt += " LIMIT " + limit
        res = c.execute(stmt)
        return res

    def remove(self, condition):
        c = self.conn.cursor()
        stmt = "DELETE FROM " + self.snapshots_table + " WHERE " + condition
        c.execute(stmt)
        self.conn.commit()

    def get_num_snapshots(self):
        c = self.conn.cursor()
        stmt = "SELECT count(1) FROM " + self.snapshots_table
        res = c.execute(stmt)
        counts = 0
        for r in res:
            counts = r[0]
        return counts

    def get_current_snapshot(self, branch):
        c = self.conn.cursor()
        stmt = "SELECT " + SnapshotTree.CURRENT + " FROM " + self.branches_table + \
               " WHERE " + SnapshotTree.BRANCH + " = ?"
        res = c.execute(stmt, (branch,))
        current_snapshot = None
        for r in res:
            current_snapshot = r[0]
        return current_snapshot

    def get_snapshot_branch(self, name):
        c = self.conn.cursor()
        stmt = "SELECT " + SnapshotTree.BRANCH + " FROM " + self.branches_table + \
               " WHERE " + SnapshotTree.CURRENT + " = ?"
        cursor = c.execute(stmt, (name,))
        res = cursor.fetchone()
        snapshot_branch = None
        if res is None:
            stmt = "SELECT " + SnapshotAttribute.BRANCH + " FROM " + self.snapshots_table + \
                   " WHERE " + SnapshotAttribute.NAME + " = ?"
            cursor = c.execute(stmt, (name,))
            res = cursor.fetchone()
            if res is not None:
                snapshot_branch = res[0]
        else:
            snapshot_branch = res[0]
        return snapshot_branch

    def put_branch_snapshot(self, branch_name, name):
        c = self.conn.cursor()
        columns = ','.join(self.branch_attribs)
        value_str = ",".join(['?' for _ in range(len(self.branch_attribs))])
        stmt = "INSERT OR REPLACE INTO " + self.branches_table + " (" + columns + ") VALUES (" + value_str + ")"
        c.execute(stmt, (branch_name, name))
        self.conn.commit()

    def remove_snapshot_info(self, name):
        c = self.conn.cursor()
        stmt = "SELECT " + SnapshotTree.BRANCH + " FROM " + self.branches_table + \
               " WHERE " + SnapshotTree.CURRENT + " = ?"
        cursor = c.execute(stmt, (name,))
        branch = cursor.fetchone()
        snapshot_branch = None
        if branch is not None:
            # find parent's branch and check if that is same as that of child's
            # then after deletion, the parent becomes the latest snapshot in the branch
            stmt = "SELECT A." + SnapshotAttribute.PARENT + ", B." + SnapshotAttribute.BRANCH + \
                   " FROM " + self.snapshots_table + " A, " + self.snapshots_table + " B " +  \
                   " WHERE A." + SnapshotAttribute.NAME + " = ? AND A." + SnapshotAttribute.PARENT + \
                   " = B." + SnapshotAttribute.NAME
            cursor = c.execute(stmt, (name,))
            res = cursor.fetchone()
            if res is not None:
                parent, parent_branch = res[0], res[1]
                print(parent, parent_branch)
                if branch == parent_branch:
                    self.put_branch_snapshot(branch, parent)
                else:
                    snapshot_branch = branch
            else:
                snapshot_branch = branch
        #stmt = "DELETE FROM " + self.branches_table + " WHERE " + SnapshotTree.CURRENT + " = ?"
        #c.execute(stmt, (name,))
        #stmt = "DELETE FROM " + self.snapshots_table + " WHERE " + SnapshotAttribute.NAME + " = ?"
        #c.execute(stmt, (name,))
        self.conn.commit()
        return snapshot_branch

    def get_connected_tags(self, snapshot_name):
        snapshot_list = self.recurse_snapshot_ids(snapshot_name)


    def recurse_snapshot_ids(self, snapshot_name, snapshot_list=[]):
        c = self.conn.cursor()
        stmt = "SELECT " + SnapshotAttribute.PARENT + " FROM " + self.snapshots_table +\
                " WHERE " + SnapshotAttribute.NAME + " = ?"
        cursor = c.execute(stmt, (snapshot_name,))
        res = cursor.fetchone()
        snapshot_list.append(snapshot_name)
        if res[0] is not None:
            parent_name = res[0]
            return self.recurse_snapshot_ids(parent_name, snapshot_list)
        else:
            stmt = "SELECT " + SnapshotAttribute.ALIAS + " FROM " + self.snapshots_table + \
                   " WHERE " + SnapshotAttribute.NAME + " IN ({})".format(','.join('?'*len(snapshot_list)))
            cursor = c.execute(stmt, snapshot_list)
            snapshot_ids = []
            res = cursor.fetchall()
            for r in res:
                snapshot_ids.append(r[0])
            return snapshot_ids


    def close(self):
        self.conn.close()
