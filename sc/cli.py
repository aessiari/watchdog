"""
`sc.cli`
====================================

.. currentmodule:: sc.cli

:platform: Unix, Mac
:synopsis: Entry-point to Science Capsule CLI

.. moduleauthor:: Devarshi Ghoshal <dghoshal@lbl.gov>

"""

import argparse
import sys

from sc.logging import setup_logging


def _addServicesParser(subparsers):
    from sc.services.cli import populate_services_parser

    parser = subparsers.add_parser('services', help='Manage Science Capsule services')
    populate_services_parser(parser)


def _add_inspect_parser(subparsers):
    from sc.inspect import populate_cli_parser

    parser = subparsers.add_parser('inspect', help='Inspect Science Capsule data and configuration')
    populate_cli_parser(parser)


def _add_bootstrap_parser(subparsers):
    from sc.bootstrap import populate_cli_parser

    parser = subparsers.add_parser('bootstrap', help='Create and populate Science Capsule configuration and services')
    populate_cli_parser(parser)


def _add_capture_parser(subparsers):
    from sc.capture.cli import populate_argument_parser

    parser = subparsers.add_parser('capture', help='Manage and trigger capture from available event sources')
    populate_argument_parser(parser)


##################################
def main():
    parser = argparse.ArgumentParser(description="",
                                     prog="sc",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    subparsers = parser.add_subparsers()
    _addServicesParser(subparsers)
    _add_inspect_parser(subparsers)
    _add_bootstrap_parser(subparsers)
    _add_capture_parser(subparsers)

    args = parser.parse_args()

    # TODO do some additional setting here e.g. from a --verbose CLI flag
    setup_logging()

    if len(args.__dict__) == 0:
        parser.print_usage()
        sys.exit(1)

    args.dispatch_func(args)


##################################
if __name__ == '__main__':
    main()
