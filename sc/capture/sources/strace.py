import functools
import json
import re
import subprocess

import pandas as pd

from sc.db import models
from sc.capture import base, monitors, analysis, util


SOURCE = 'strace'


class StraceRawEvent(models.RawEvent):
    "Event produced by capturing the output of strace(1)"

    event_data = models.fields.CharField(blank=True, required=True)

    objects = models.UtilManager()
    staged = models.StagingManager()
    incremental = models.IncrementalManager(range_field='timestamp_ns')


class Syscalls:
    execve = 'execve'
    exit_group = 'exit_group'
    for_proc_analysis = [execve, exit_group]


class Monitor(monitors.SubprocessOutputMonitor):
    event_model = StraceRawEvent

    def configure(self, pids=None):
        args = ['strace']

        syscalls = []
        syscalls += Syscalls.for_proc_analysis

        syscalls_arg = str.join(',', syscalls)

        args += [
            '-f',
            '-ttt',
            '-y',  # print absolute path together with file descriptor
            '-e', f'trace={syscalls_arg}',
            '-e', 'verbose=all',
            '-e', 'abbrev=none',
            '-s', '9999',  # max length of displayed values
            # '-qq',  # do not print messages and process error codes
            '-e', 'signal=none'  # do not print non-syscall signal info
        ]

        pids = pids or []
        assert pids, 'At least one PID should be given'

        for pid in pids:
            args += ['-p', str(pid)]

        self.cli_args = args
        self.log.debug('cli_args: %s', self.cli_args)

    @property
    def subprocess_kwargs(self):
        base_opts = super().subprocess_kwargs
        return dict(base_opts, stderr=subprocess.STDOUT)


class ParseExecveSyscallArgs(analysis.LinearPipeline):

    truncated_syscall_value_regex = re.compile(r'["][.]{3}|[.]{3}$')
    truncated_marker = '<TRUNCATED>'
    null_value_replacement = '"", [], []'

    def sanitize_truncated(self, s):
        return s.str.replace(self.truncated_syscall_value_regex, f'{self.truncated_marker}"', regex=True)

    def convert_empty(self, s):
        return (s
                .fillna(self.null_value_replacement)
               )

    def convert_to_json_array(self, s):
        return '[' + s + ']'

    def parse_execve_args_str(self, s):
        try:
            extracted = json.loads(s)
        except json.JSONDecodeError:
            extracted = self.fallback_parse_execve_args_str(s)
        return extracted

    def fallback_parse_execve_args_str(self, s):
        sep = ', '
        exepath, rest = s.split(sep, 1)
        # at this point "exepath" should look like ["/path/to/exe"
        exepath = re.sub(r'^\[\"', '', exepath)
        exepath = re.sub(r'\"$', '', exepath)

        return exepath, None, None

    def extract_args_parts(self, s):
        return s.apply(self.parse_execve_args_str)

    def to_dataframe(self, s):
        return (s
                .pipe(util.expand_str_series_to_df, names=['exepath', 'argv', 'env_info'])
               )

    @property
    def default_steps(self):
        return [
            self.sanitize_truncated,
            self.convert_empty,
            self.convert_to_json_array,
            self.extract_args_parts,
            self.to_dataframe,
        ]


class RawEventsProcessingPipeline(analysis.LinearPipeline):

    strace_timestamp_unit = 's'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._proc_properties_history = []

    @property
    def event_cls(self):
        return self.params.get('event_cls')

    class c:
        event_data = 'event_data'
        syscall_data = 'syscall_data'
        syscall_args = 'syscall_args'
        syscall_ret_code = 'syscall_ret_code'
        timestamp = 'timestamp'
        ts = 'ts'
        pid = 'pid'
        syscall_name = 'syscall_name'
        proc_exepath = 'proc_exepath'
        proc_data = 'proc_data'
        action = 'action'

    def parse_strace_output(self, df):
        parser = util.RegexParser(
            field=self.c.event_data,
            regex=r'(?:\[pid\s+(?P<pid>\d+)\])?\s?(?P<timestamp>\d*[.]\d*)\s+(?P<syscall_data>.+$)',
        )

        return df.pipe(parser.extract_and_join)

    def parse_syscall_data(self, df):
        parser = util.RegexParser(
            field=self.c.syscall_data,
            regex=r'(?P<syscall_name>\w+)[(](?P<syscall_args>.*)?[)](?:\s+=\s+(?P<syscall_ret_code>\w+))?'
        )

        return df.pipe(parser.extract_and_join)

    def upcast_dtypes(self, df):
        get_timestamp = functools.partial(util.get_timestamp_from_epoch, unit=self.strace_timestamp_unit)

        assigns = {
            self.c.ts: lambda d: d[self.c.timestamp].pipe(get_timestamp),
            self.c.pid: lambda d: d[self.c.pid].fillna(-1).astype(int)
        }

        return df.assign(**assigns)

    def is_good_line_for_proc_analysis(self, df):
        syscall_name = df[self.c.syscall_name]

        is_good_execve = (syscall_name == Syscalls.execve) & (df[self.c.syscall_ret_code] == '0')
        is_good_exit = (syscall_name == Syscalls.exit_group) & (df[self.c.syscall_args].notnull())

        return is_good_execve | is_good_exit

    def select_process_data(self, df):

        def is_process_info(x):
            return x.isin(Syscalls.for_proc_analysis)

        fields_to_keep = [
            self.c.ts,
            self.c.pid,
            self.c.syscall_name,
            self.c.syscall_args,
            self.c.syscall_ret_code,
            # self.c.proc_exepath,
            # self.c.proc_data
        ]

        return (df
                # [lambda d: d[self.c.syscall_name].pipe(is_process_info)]
                [self.is_good_line_for_proc_analysis]
                [fields_to_keep]
               )

    def extract_proc_info_from_syscall_args(self, s):
        return ParseExecveSyscallArgs().run(s)

    def get_proc_properties(self, df):

        fields_needed_for_proc_info = [
            self.c.pid,
            self.c.syscall_args
        ]

        def has_usable_proc_info(d):
            return (d[self.c.syscall_name] == Syscalls.execve) & (d[self.c.syscall_ret_code] == '0')

        def incorporate_parsed_proc_data(d):
            proc_data = (d
                         [self.c.syscall_args]
                         .pipe(self.extract_proc_info_from_syscall_args)
                        )

            return d.join(proc_data)

        return (df
                [has_usable_proc_info]
                [fields_needed_for_proc_info]
                .pipe(incorporate_parsed_proc_data)
                .set_index('pid')
                .dropna(subset=['exepath'])
                .drop(columns=[self.c.syscall_args])
                # TODO here we would add the other extracted properties
                # [prop_fields]
               )

    def get_proc_properties_with_history(self, df):
        current = self.get_proc_properties(df)

        # we can append even if current is empty, since they will be concat()-ed anyway
        self._proc_properties_history.append(current)

        return pd.concat(self._proc_properties_history)

    def incorporate_proc_properties(self, df):
        # this data belongs to a separate table and should be joined/linked with the events
        # this data should be kept between interactions and updated with new information
        proc_properties = (self.get_proc_properties_with_history(df)
                           .rename(columns=lambda c: f'proc_{c}')
                          )

        drop_suffix = '_to_drop'

        merge_opts = {
            'left_on': self.c.pid,
            'right_index': True,
            # "inner" works well if we have access to proc properties with history,
            # otherwise using e.g. "left" there seem to be several exit_group false positives
            'how': 'inner',
            'suffixes': (drop_suffix, '')
        }

        def drop_with_suffix(d):
            to_drop = [c for c in d if c.endswith(drop_suffix)]
            return d.drop(columns=to_drop)

        # self.log.debug('df.columns=%s', df.columns)
        # self.log.debug('proc_properties.columns=%s', proc_properties.columns)

        return (df
                .merge(proc_properties, **merge_opts)
                .pipe(drop_with_suffix)
               )

    def assing_event_properties(self, df):
        # don't need the copy for the moment since we're not modifying the df directly
        # df = df.copy()
        # TODO use enums instead
        map_syscall_to_action = {
            Syscalls.execve: models.process.Action.start,
            Syscalls.exit_group: models.process.Action.exit,
        }

        assigns = {
            self.c.action: lambda d: d[self.c.syscall_name].map(map_syscall_to_action)
        }

        return df.assign(**assigns)

    def get_event_from_dataframe_row(self, row, event_cls=None):
        e = event_cls()

        e.time = row[self.c.ts]

        e.action = row[self.c.action]
        e.artifact_type = models.process.ArtifactType.process

        # e.artifact = {
        #     'type': e.artifact_type,
        #     'pid': row[self.c.pid],
        #     'exepath': row[self.c.proc_exepath],
        #     'argv': row['proc_argv']
        # }
        e.artifact.type = e.artifact_type
        e.artifact.pid = row[self.c.pid]
        e.artifact.exepath = row[self.c.proc_exepath]
        e.artifact.argv = row['proc_argv']

        e.source = SOURCE

        return e

    def create_event_objects(self, df):
        return util.create_objects_from_dataframe(
            df,
            self.get_event_from_dataframe_row,
            event_cls=self.event_cls
        )

    def display(self, df):
        print(df)
        return df

    @property
    def default_steps(self):
        return [
            self.parse_strace_output,
            self.parse_syscall_data,
            self.upcast_dtypes,
            self.select_process_data,
            self.incorporate_proc_properties,
            self.assing_event_properties,
            self.create_event_objects,
        ]


class Analyzer(analysis.RawEventsAnalyzer):
    raw_event_model = StraceRawEvent
    event_model = models.ProcessEvent

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.processor = RawEventsProcessingPipeline(event_cls=self.event_model)


class Adaptor(base.Adaptor):
    source = SOURCE

    def __init__(self, *args, monitor=None, ingester=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.monitor = monitor or Monitor()
        self.ingester = ingester or Analyzer()

    def configure(self, settings):
        self.monitor.configure(
            pids=settings.monitored_pids
        )
