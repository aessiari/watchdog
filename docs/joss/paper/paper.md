---
title: Science Capsule - Capturing the Data Life Cycle
tags:
  - reproducibility
  - scientific workflows
  - data management
  - high performance computing
  - big data analytics
authors:
 - name: Devarshi Ghoshal
   orcid: 0000-0002-6819-6949
   affiliation: 1
 - name: Ludovico Bianchi
   orcid: 0000-0002-0527-1534
   affiliation: 1
 - name: Abdelilah Essiari
   affiliation: 1
 - name: Michael Beach
   affiliation: "1, 2"
 - name: Drew Paine
   orcid: 0000-0003-0711-9744
   affiliation: 1
 - name: Lavanya Ramakrishnan
   orcid: 0000-0003-1761-4132
   affiliation: 1
affiliations:
 - name: Lawrence Berkeley National Lab
   index: 1
 - name: University of Washington
   index: 2
date: 12 June 2020
bibliography: paper.bib
---

# Summary

Reproducibility and sharing of scientific data processing and analyses is recognized as being critical. A key to enabling reproducibility and sharing is capturing the scripts, the software environment, context, and provenance. Existing tools (@guo2012burrito, @chirigati2016reprozip, @brinckman2019computing, @vsimko2019reana) require researchers to either modify or instrument their analyses, which is a barrier to use. 

Science Capsule is a free open source software that researchers can use to capture the entire end-to-end data processing and analyses life cycle including the scripts, data, and execution environment. Science Capsule provides a monitoring environment that captures the provenance at runtime that is available to users through a timeline view in a web interface. Science Capsule leverages container technologies to provide a lightweight, executable package of applications and all required dependencies to allow sharing and ensure reproducibility during execution. 

The Science Capsule software provides key features to allow the capture of data processing and analyses environments.  

* Captures the scripts and context of the analyses by using container technologies
* Captures and processes the provenance through file system and process monitoring 
* Designed to run on a user’s desktop and is also compatible with container technologies such as Shifter (@gerhardt2017shifter) running on HPC environments
* Provides a timeline view of the execution of the workflow through a web interface 
* Provides an interface to add artifacts including lab notebooks and notes
* Implemented in Python and supports multiple OS platforms including Windows, Mac and Linux


# Statement of Need 

Science Capsule is a software package that is designed to fit the needs of scientists with data processing and data analyses workflows to capture their software environment and provenance without requiring any changes to their code. Scientists with data processing or data analyses pipelines will benefit from the Science Capsule software package. For example, a scientist using experimental facilities such as light sources or electron microscopes will be able to capture their data analyses environment during experiment time and reuse it for post-analyses of the data or share the workflows with other researchers. 

Users can download the Science Capsule software and easily set it up to capture their workflow and capture provenance at runtime. The timeline view provides a visualization for the scientist to understand workflow execution behavior. Science Capsule lets a scientist capture their workflow to share, reproduce or port it to another hardware. 

# Science Capsule Software

Science Capsule supports two modes for capturing the information about the workflows and the associated provenance: a) container mode, where Science Capsule captures metadata for all the processes and artifacts that are encapsulated within a Docker container, and b) bare-metal mode, where Science Capsule captures the execution time provenance for user-specified artifacts. 

## Automatic Monitoring and Capture
Science Capsule monitors the environment where the workflows are managed by the researchers by using different system-level file and process monitoring tools. The use of these tools depend on the underlying platform and the granularity at which researchers intend to capture the information. Currently, Science Capsule captures events from inotify (@fisher2017linux), Linux's strace utility and the Python watchdog library. These tools are configured and set up during the installation of Science Capsule. These raw events captured by the various monitoring tools are processed to extract the high-level data life cycle and task execution information. Finally, both raw and processed events are stored in a mongodb database for sharing and visualizing the scientific pipeline. 

## Interactive Web Interface
The processed events in Science Capsule are used to represent various activities of a scientific pipeline in a chronological order, a.k.a. the timeline. Science Capsule provides an interactive web interface where researchers can view the timeline in near real-time and/or annotate with notes/images that might capture a researcher's thought process and experiment design. The interactive web interface provides a way for the researchers to enrich the execution and provenance information collected through the monitoring framework for enabling better reusability and sharing of knowledge. Figure 1 shows an example timeline for a workflow as captured by Science Capsule.

![Science Capsule user interface.\label{fig:example_webui}](figs/example_webui.png) Figure 1: Science Capsule web user interface.

# Related Work

Over the past decade, several frameworks have been developed to enable computational reproducibility. Burrito (@guo2012burrito) captures a researcher’s computational activities and provides user interfaces to annotate the captured provenance. Other tools provide specific wrappers and dedicated interfaces to create reproducible packages for software-based experiments and computational narratives (@chirigati2016reprozip, @brinckman2019computing). REANA (@vsimko2019reana) provides a platform for defining and managing reusable workflows through cloud computing. Unlike Science Capsule, these tools often require modifications to researchers’ existing work practices and do not allow users to capture ad-hoc resources (e.g., lab notebooks etc). 

Metadata and provenance are critical in building knowledge for enabling reproducibility of scientific workflows. Past research has shown the use of data provenance for sharing and reproducing scientific workflows (@goble2010myexperiment, fomel2013madagascar). However, existing workflow management systems (@oinn2004taverna, @deelman2015pegasus, @altintas2006provenance, barga2006automatic) are explicitly instrumented for capturing data provenance from scientific workflows. Science Capsule is agnostic to workflow tools and uses system-level monitoring to extract information from experimental processes and artifacts. It augments and complements existing real world practices and tools without requiring a wholesale adaptation of scientist's work to fit the design of Science Capsule, while providing usable and understandable interfaces for engaging with detailed provenance information.

Additionally, scientists and researchers may not be using existing workflow tools for managing their scientific pipelines, which makes the case for other alternatives for collecting relevant metadata to enable reproducibility. Traditionally, filesystem metadata have been used for performance monitoring and anomaly detection (@miller2010monitoring, @muniswamy2006provenance, @huang2011anomaly). Metadata and data context services like Ground (@hellersteinground) and Bluesky data broker (@arkilic2015databroker) provide integrated interfaces to access data and metadata from various sources. We envision Science Capsule to use metadata collected from these systems to enrich the provenance in addition to what is automatically captured through system-level monitoring tools in Science Capsule . 

# Acknowledgements
This work is supported by the U.S. Department of Energy,
Office of Science and Office of Advanced Scientific Computing
Research (ASCR) under Contract No. DE-AC02-05CH11231.

# References