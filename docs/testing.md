## Running the test suite

From the root of the cloned Science Capsule repository,
after activating the environment where Science Capsule is installed,
run the following command to install the package with the additional dependencies needed for running the unit tests:

```sh
# the quotes might be needed to avoid the square brackets to be interpreted by the shell
python -m pip install -e '.[tests]'
```

Then, invoke the `pytest` command to run the test suite:

```sh
pytest tests/
```
