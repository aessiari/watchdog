#!/usr/bin/env python

from setuptools import setup, find_packages
import sys
import os

python_version = sys.version_info


with open('VERSION') as f:
    version = f.readline().strip()


setup(name='sc',
      version=version,
      description='Science Capsule',
      author='Devarshi Ghoshal',
      author_email='dghoshal@lbl.gov',
      keywords='',
      packages=find_packages(exclude=['ez_setup']),
      include_package_data=True,
      zip_safe=False,
      classifiers=['Development Status :: 1 - Alpha',
                   'Intended Audience :: Science/Research',
                   'Natural Language :: English',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python :: 3.6',
                   'Topic :: Scientific/Engineering',
                   'License :: OSI Approved :: 3-clause BSD License'
      ],
      install_requires=[
          'pyyaml',
          'pymongo',
          'pandas==0.25.*',  # TODO pandas could probably be updated to the latest version
          'watchdog',
          'psutil; platform_system!="Windows"',
          # FIXME Windows has problem with the latest (5.7) version of psutil
          # see also https://github.com/giampaolo/psutil/issues/1662
          # pinning to a lower version while the fix is available on PyPI/conda-forge
          'psutil<5.6; platform_system=="Windows"',
          'supervisor; platform_system!="Windows"',
          'supervisor-win>=4.3.0; platform_system=="Windows"',
          # IMPORTANT to support stopping services with CTRL_BREAK_EVENT,
          # supervisor-win needs to be installed from the GitHub repo rather than PyPI
          # see https://github.com/alexsilva/supervisor/issues/17
          # FIXME this syntax should work following PEP 508, but it seems to be incompatible with the `platform_system` spec
          # 'supervisor-win@git+https://github.com/alexsilva/supervisor@b3ea562e436cec39bf8e390aa936aca1884960c1#egg=supervisor_win; platform_system=="Windows"',
          'tabulate',
          'gitpython',
          'pymodm'
      ],
      extras_require={
          'tests': [
              'pytest>=4.4.1',
              'mongomock==3.19.0',
          ]
      },
      entry_points={
          'console_scripts': [
              'sc = sc.cli:main',
              'sc-bootstrap = sc.bootstrap:main'
        ]
      },
      data_files=[#(os.path.join(scdir, 'config'), ['config/logging.yaml']),
                  ('', ['VERSION'])]
)
