# [![Science Capsule](docs/sc_logo.png)](http://sciencecapsule.lbl.gov)

### About

[Science Capsule](http://sciencecapsule.lbl.gov) is a framework to understand, reuse and reproduce a scientific workflow. It helps scientists capture the data and knowledge about the life cycle of their scientific experiments or workflows, including the processes, datasets and other relevant artifacts. Science Capsule supports workflows running on different OS platforms and on containers.

Science Capsule is motivated by workflows combining tasks and data from the [Advanced Light Source (ALS)](https://als.lbl.gov/) and [The Materials Project (MP)](https://materialsproject.org/), but the design is applicable to a number of different scientific domains and workflows. Science Capsule
works with existing workflow tools and scripts, and unlike  existing frameworks that require researchers to either modify or instrument their workflows, can be used out-of-the-box for their research.

### Features

* Automatic capture of workflow events with minimal human intervention
* Ability to collect events from multiple event sources
* Capability to create, save, share, and extend workflows using containers
* Allows for easy migration and execution of workflows across multiple platforms
* A simple interface to annotate workflow executions with user notes and images

### Supported platforms

*   Linux
*   Mac OS X
*   Windows

In addition, Science Capsule is designed to work on:

* HPC systems (e.g., the [Cori supercomputer](https://docs.nersc.gov/systems/cori/) at NERSC)
* Docker containers
* HPC containers (e.g., [Shifter](https://github.com/NERSC/shifter))

### Getting started

You can follow the links below to install Science Capsule:

* on [bare-metal](docs/README-baremetal.md) 
* with [containers](docs/README-docker.md)

Once installed, you can:

* run the [test suite](docs/testing.md)
* follow the guide for running an [example workflow](docs/example.md)

As the workflow events are captured in Science Capsule, you can [follow these steps to view the timeline](docs/webui.md) on a web browser.

### License

[BSD-3-Clause-LBNL](https://bitbucket.org/sciencecapsule/sciencecapsule/src/master/LICENSE)

